/**
 * Created by rt.yishuangxi on 2014/9/24.
 */
module.exports = function (grunt) {

    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        uglify: {
            options: {
                mangle: true,
                sourceMap: true,
                compress: {
                    drop_console: true
                },
                banner: '/*! hello there : <%= pkg.name %> - v<%= pkg.version %> - ' +
                    '<%= grunt.template.today("yyyy-mm-dd") %> */'
            },
            my_target: {
                files:[{
                    expand:true,
                    src:"src/*.js",
                    dest:"../"
                }]
            }
        },
        jshint:{
            all:["src/YI.js"]
        }
    });

    // Load the plugin that provides the "uglify" task.
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    // Default task(s).
    grunt.registerTask('default', ['uglify']);

};